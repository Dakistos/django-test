import os

from django.test import TestCase
from . import views
from django.urls import resolve
# Create your tests here.
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

from .models import IrisData


class IrisDataTestCase(TestCase):
    """
    Gestion des modèles voir les methodes dans models.py
    """

    """
    Gestion des redirections voir les redirections dans server/urls.py et server/predict.urls.py
    """
    # du code ici
    def test_predict(self):
        found = resolve('/api/')
        self.assertEqual(found.func, views.predict)

        found = resolve('/results/')
        self.assertEqual(found.func, views.view_results)

    """
    Gestion avec la base de données: vérifier que lors d'un save() la donnée et bien disponible dans le BDD
    """
    # du code ici

    ## en bonus, vous pouvez vérifier le forms.py

    """
    Selenium
    """

    def setUp(self):
        self.driver = webdriver.Chrome('E:/aaaaaaaa/chromedriver.exe')

    def test_search(self):
        driver = self.driver
        driver.set_window_size(1920, 1080)
        driver.get("http://localhost:8000/admin/")
        admin = driver.find_element_by_tag_name('a').text
        print(admin, " / affichage OK page admin")